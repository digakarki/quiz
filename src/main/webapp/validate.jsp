<link rel="stylesheet" href="./css/quiz.css" />

<div class="result-container">
    <h2> Result </h2>
    <%
        String[] answers = {"32 and 64", "Int to long", "Objects", "static", "java.lang.Object", "int value", "java.lang", "13", "void", "3.0"};

        String q1 = request.getParameter("q1");
        String q2 = request.getParameter("q2");
        String q3 = request.getParameter("q3");
        String q4 = request.getParameter("q4");
        String q5 = request.getParameter("q5");
        String q6 = request.getParameter("q6");
        String q7 = request.getParameter("q7");
        String q8 = request.getParameter("q8");
        String q9 = request.getParameter("q9");
        String q10 = request.getParameter("q10");

        String[] userAnswers = {q1, q2, q3, q4, q5, q6, q7, q8, q9, q10};

        for(int i=0; i < answers.length; i++){
            %>
                <div> Your Answer for Question <%= i+1 %> is <b><%= userAnswers[i] %></b> </div>
            <%
            if (answers[i].equals(userAnswers[i])) { %>
                <span class="correct"> Status: Correct </span><br/><br/>
            <% }
            else { %>
                <span class="incorrect"> Status: InCorrect </span><br/>
                <span>Correct Answer: <%= answers[i] %> </span><br/><br/>
            <%}
        }
    %>
</div>