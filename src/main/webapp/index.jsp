<%-- 
    Document   : index
    Created on : Feb 22, 2022, 10:27:42 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="./css/quiz.css" />
    </head>
    <body>
        <form action="validate.jsp">
            <div class="container">
                <h2> Online Quiz </h2>
                <div>
                    1. What is the size of float and double in java??
                    <br/>
                    <input type="radio" name="q1" value="32 and 64">32 and 64</input>           
                    <input type="radio" name="q1" value="32 and 32">32 and 32</input>
                    <input type="radio" name="q1" value="64 and 64">64 and 64</input>
                    <input type="radio" name="q1" value="64 and 32">64 and 32</input>
                    <br/>
                </div>

                <div>
                    2. Automatic type conversion is possible in which of the possible cases?
                    <br/>
                    <input type="radio" name="q2" value="Byte to int">Byte to int</input>           
                    <input type="radio" name="q2" value="Int to long">Int to long</input>
                    <input type="radio" name="q2" value="Long to int">Long to int</input>
                    <input type="radio" name="q2" value="Short to int">Short to int</input>
                    <br/>
                </div>

                <div>
                    3. Arrays in java are:
                    <br/>
                    <input type="radio" name="q3" value="Object references">Object references</input>           
                    <input type="radio" name="q3" value="Objects">Objects</input>
                    <input type="radio" name="q3" value="Primitive data type">Primitive data type</input>
                    <input type="radio" name="q3" value="None">None</input>
                    <br/>
                </div>

                <div>
                    4. What keyword makes a variable belong to a class,rather than being defined for each instance of the class?
                    <br/>
                    <input type="radio" name="q4" value="final">final</input>           
                    <input type="radio" name="q4" value="static">static</input>
                    <input type="radio" name="q4" value="volatile">volatile</input>
                    <input type="radio" name="q4" value="abstract">abstract</input>
                    <br/>
                </div>

                <div>
                    5. In which of the following is toString() method defined?
                    <br/>
                    <input type="radio" name="q5" value="java.lang.Object">java.lang.Object</input>           
                    <input type="radio" name="q5" value="java.lang.String">java.lang.String</input>
                    <input type="radio" name="q5" value="java.lang.util">java.lang.util</input>
                    <input type="radio" name="q5" value="none">none</input>
                    <br/>
                </div>

                <div>
                    6. compareTo() returns
                    <br/>
                    <input type="radio" name="q6" value="True">True</input>           
                    <input type="radio" name="q6" value="False">False</input>
                    <input type="radio" name="q6" value="int value">int value</input>
                    <input type="radio" name="q6" value="none">none</input>
                    <br/>
                </div>

                <div>
                    7. To which of the following does the class string belong to?
                    <br/>
                    <input type="radio" name="q7" value="java.lang">java.lang</input>           
                    <input type="radio" name="q7" value="java.awt">java.awt</input>
                    <input type="radio" name="q7" value="java.applet">java.applet</input>
                    <input type="radio" name="q7" value="java.string">java.string</input>
                    <br/>
                </div>

                <div>
                    8. Total constructor string class have?
                    <br/>
                    <input type="radio" name="q8" value="3">3</input>           
                    <input type="radio" name="q8" value="7">7</input>
                    <input type="radio" name="q8" value="13">13</input>
                    <input type="radio" name="q8" value="20">20</input>
                    <br/>
                </div>

                <div>
                    9. Identify the return type of a method that does not return any value.
                    <br/>
                    <input type="radio" name="q9" value="int">int</input>           
                    <input type="radio" name="q9" value="void">void</input>
                    <input type="radio" name="q9" value="double">double</input>
                    <input type="radio" name="q9" value="none">none</input>
                    <br/>
                </div>

                <div>
                    10. Output of Math.floor(3.6)?
                    <br/>
                    <input type="radio" name="q10" value="3">3</input>           
                    <input type="radio" name="q10" value="3.0">3.0</input>
                    <input type="radio" name="q10" value="4">4</input>
                    <input type="radio" name="q10" value="4.0">4.0</input>
                    <br/>
                </div>

                <input type="submit">
            </div>
        </form>
    </body>
</html>
